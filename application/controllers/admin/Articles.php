<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Articles extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("article_model");
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data["articles"] = $this->article_model->getAll();
        $this->load->view("admin/article/list", $data);
    }

    public function add()
    {
        $article = $this->article_model;
        $validation = $this->form_validation;
        $validation->set_rules($article->rules());

        if ($validation->run()) {
            $article->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $this->load->view("admin/article/new_form");
        
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/articles');
       
        $article = $this->article_model;
        $validation = $this->form_validation;
        $validation->set_rules($article->rules());

        if ($validation->run()) {
            $article->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["articles"] = $article->getById($id);
        if (!$data["articles"]) show_404();
        
        $this->load->view("admin/article/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->article_model->delete($id)) {
            redirect(site_url('admin/articles'));
        }
    }
}