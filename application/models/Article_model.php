<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Article_model extends CI_Model
{
    private $_table = "articles";

    public $article_id;
    public $nama_article;
    public $image = "default.jpg";
    public $description_article;

    public function rules()
    {
        return [
            ['field' => 'nama_article',
            'label' => 'Nama_article',
            'rules' => 'required'],

           
            
            ['field' => 'description_article',
            'label' => 'Description_article',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["article_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->article_id = uniqid();
        $this->nama_article = $post["nama_article"];
        $this->image = $this->_uploadImage();
        $this->description_article = $post["description_article"];
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->article_id = $post["id"];
        $this->nama_article = $post["nama_article"];
        //$this->image = $post["image"];
        if (!empty($_FILES["image"]["name"])) {
    $this->image = $this->_uploadImage();
} else {
    $this->image = $post["old_image"];
}
        $this->description_article = $post["description_article"];

        return $this->db->update($this->_table, $this, array('article_id' => $post['id']));
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("article_id" => $id));
    }

    private function _uploadImage()
{
    $config['upload_path']          = './upload/article-image/';
    $config['allowed_types']        = 'gif|jpg|png';
    $config['file_name']            = $this->article_id;
    $config['overwrite']            = true;
    $config['max_size']             = 10024; // 1MB
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('image')) {
        return $this->upload->data("file_name");
    }
    
    return "default.jpg";
}
    private function _deleteImage($id)
{
    $article = $this->getById($id);
    if ($article->image != "default.jpg") {
        $filename = explode(".", $article->image)[0];
        return array_map('unlink', glob(FCPATH."upload/article-image/$filename.*"));
    }
}
}